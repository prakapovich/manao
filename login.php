<?php

if (!isset($_POST['submit'])) {
    die('Ошибка запроса');
}

$login = $_POST['login'];
$password = $_POST['password'];

require_once ('md5_sault.php');

//По логину находим пароль и сравниваем с введенным
$valid = new DOMDocument();
$valid->load('db/db.xml');
$xpath = new DOMXPath($valid);
$passwords_valid = $xpath->query("/users/users/user[login='$login']/password");

foreach ($passwords_valid as $password_valid) {
    if ($password_valid->nodeValue != $password) {
        die('Неверный пароль');
    }
}

//проверка логина на регистрацию
$xml = simplexml_load_file('db/db.xml');

if (array_search($login, $xml->xpath('//login'))) {
    echo 'Hello ' . $login;
} else {
    die('Введенный логин не зарегистрирован: '. $login);
}