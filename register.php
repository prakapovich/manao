<?php


if (!isset($_POST['submit'])) {
    die('Ошибка запроса');
}


$login = $_POST['login'];
$password = $_POST['password'];
$confirm_password = $_POST['confirm_password'];
$email = $_POST['email'];
$name = $_POST['name'];


//сравниваем пароли
if ($password != $confirm_password) {
    die('Пароль не совпадает');
}

require_once ('md5_sault.php');


//проверка на уникальность логина
$xml = simplexml_load_file('db/db.xml');

if (array_search($login, $xml->xpath('//login'))) {
    die('Логин занят! ' . $login);
}

//проверка на уникальность мыла
if (array_search($email, $xml->xpath('//email'))) {
    die('Email занят! ' . $email);
}
/////////////////////////ЛАГ С ЛОГИНОМ и мылом которые первые в бд: регистрируется бесконечно, пересмотреть запись в БД

//запись в БД
$db = new DOMDocument();
$db->load('db/db.xml');
$users = $db->createElement('users');
$user = $db->createElement('user');

$login_db = $db->createElement('login');
$password_db = $db->createElement('password');
$email_db = $db->createElement('email');
$name_db = $db->createElement('name');

$login_db_text = $db->createTextNode($login);
$login_db->appendChild($login_db_text);
$user->appendChild($login_db);

$password_db_text = $db->createTextNode($password);
$password_db->appendChild($password_db_text);
$user->appendChild($password_db);

$email_db_text = $db->createTextNode($email);
$email_db->appendChild($email_db_text);
$user->appendChild($email_db);

$name_db_text = $db->createTextNode($name);
$name_db->appendChild($name_db_text);
$user->appendChild($name_db);

$users->appendChild($user);
$db->documentElement->appendChild($users);


$db->formatOutput = true;

$db->save('db/db.xml');

echo 'Вы зарегистрировались под логином ' . $login;